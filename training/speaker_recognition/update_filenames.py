"""🎤 Meetup Data Science Brest 2021 - Rename Filenames

Authors
  * Jean-Marie Prigent
  * Thomas Chaigneau
"""

from os import listdir, rename
from os.path import isfile, isdir, join

def list_all_files(directory:str):
    """Permet de lister tous les fichiers d'un répertoire."""
    if isdir(directory):
        files = [f for f in listdir(directory) if isfile(join(directory, f))]
        return files
    else: raise Exception(f"{directory} is not a valid directory.")

def rename_files(speaker_id: int, directory: str):
    """Permet de renommer tous les fichiers d'un répertoire."""
    files = list_all_files(directory)
    for file in files:
        rename(join(directory, file), join(directory, f"{speaker_id}-{file}"))

def exec_multiple_dir(list_of_dir: list):
    """Exécution sur plusieurs répertoires."""
    for i, directory in enumerate(list_of_dir):
        rename_files(i, directory)

# Processus de renommage des fichiers des répertoires du dataset
directories = [
    "training/speaker_recognition/data/jeanmarie_records",
    "training/speaker_recognition/data/thomas_records"
]
exec_multiple_dir(directories)