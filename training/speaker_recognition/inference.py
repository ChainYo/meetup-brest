import torchaudio
import torch
import speechbrain as sb
from speechbrain.pretrained import EncoderClassifier

classifier = EncoderClassifier.from_hparams(source="ChainYo/speaker-recognition-meetup-v3")

audio_file = "training/speaker_recognition/data/thomas_records/1-20.flac"
# audio_file = "training/speaker_recognition/data/jeanmarie_records/0-4.flac"
signal, fs = torchaudio.load(audio_file) # test_speaker: 1

info = torchaudio.info(audio_file)
sig = sb.dataio.dataio.read_audio(audio_file)

if info.num_channels > 1:
    sig = torch.mean(sig, dim=1)
resampled = torchaudio.transforms.Resample(
    info.sample_rate, 16000,
)(sig)
embeddings = classifier.encode_batch(resampled)
output_probs, score, index, text_lab = classifier.classify_batch(resampled)
print(output_probs)
print(score)
print(index)
print(text_lab)
print(f"Predicted: {text_lab[0]}")
