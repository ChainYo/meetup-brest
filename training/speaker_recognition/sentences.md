# Création d'une liste de phrases pour l'apprentissage du modèle de reconnaissance d'interlocuteur

1 | Je suis un interlocuteur.
2 | Bonjour, je suis un citoyen français.
3 | Merci pour l'invitation.
4 | J'ai besoin d'aide.
5 | Tu as de belles lunettes.
6 | Elles valent beaucoup de l'argent.
7 | Le prix n'est pas quelque chose d'important.
8 | Je ne suis pas un robot.
9 | Les gens sont gentils.
10 | Peut-être pourrais-tu m'envoyer un message.
11 | Il faut que j'appelle ce numéro de téléphone.
12 | Dormir n'est vraiment pas conseillé par le médecin.
13 | Le ministre a fait une demande de financement.
14 | Il est temps de se mettre à travailler.
15 | L'avenir est proche.
16 | J'ai réussi à créer ce site web.
17 | Pardon mais je n'ai pas bien compris la demande.
18 | Je ne peux pas vous aider.
19 | S'agit-il d'une question de politique?
20 | Il est facile de critiquer un article.
21 | Faut-il vraiment que je vous apprenne à écrire?
22 | Le brossage des dents est une bonne chose.
23 | Au moins deux à trois heures par jour.
24 | Si tu lisais ce livre, tu pourrais avoir des réponses.
25 | La lecture est un moyen de communication.
26 | L'écriture est un outil d'expression.
27 | Au revoir, je vais me coucher.
28 | Mes parents aiment beaucoup ce genre de musique.
29 | J'aime jouer du piano.
30 | Oui, je suis un homme.
31 | Elle m'a fait signe de me taire.
32 | Il est possible de faire des courses.
33 | Demain je serai en vacances.
34 | La dernière fois tu as été absente du bureau.
35 | C'est une bonne idée.
36 | Les chiens sont des animaux.
37 | Le prochain quiz sera le mercredi.
38 | Quand a-t-on notre prochain rendez-vous?
39 | Ta mère aime faire de la peinture.
40 | Nous avons de belles plantes dans le jardin.
41 | Êtes-vous disponible pour une réunion?
42 | Ben voyons, et puis quoi encore?