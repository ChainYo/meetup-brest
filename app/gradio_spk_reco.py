"""🎤 Meetup Data Science Brest 2021 - Gradio Speaker Recognition

Authors
  * Jean-Marie Prigent
  * Thomas Chaigneau
"""

import torch
import torchaudio
import gradio as gr

from typing import List
from speechbrain.pretrained import EncoderClassifier


class GradioService():
    """Service de présentation du modèle de reconnaissance de conférencier."""

    def __init__(self, source: str = None):
        """
        Initialisation du service.

        Parameters
        ----------
        source : str
            Chemin vers le fichier de configuration du modèle.
            Par défaut, le modèle utilisé est celui entrainé spécialement pour
            le Meetup Data Science Brest 2021.
        """
        if not source:
            source = "ChainYo/speaker-recognition-meetup"
        
        self.classifier = EncoderClassifier.from_hparams(source=source)
        self.rate = 16000
        self.speakers = {
            "0": "Jean-Marie Prigent",
            "1": "Thomas Chaigneau",
            "2": "Autre intervenant"
        }


    def inference(self, audio) -> str:
        """Fonction d'inférence permettant d'obtenir une prédiction sur le conférencier.
        
        Parameters
        ----------
        audio : torch.Tensor
            Audio à prédire.

        Returns
        -------
        str
            Prédiction sur le nom du conférencier.
        """

        freq = audio[0]
        audio = torch.from_numpy(audio[1]).float()

        signal = self._resample(freq, audio)

        _, _, _, text_lab = self.classifier.classify_batch(signal)

        return self._define_speaker(text_lab[0])


    def _resample(self, original_freq: int, audio: torch.Tensor) -> torch.Tensor:
        """Fonction de rééchantillonnage.

        Parameters
        ----------
        original_freq : int
            Fréquence d'origine de l'audio.
        audio : torch.Tensor
            Audio à rééchantillonner.

        Returns
        -------
        torch.Tensor
            Audio rééchantillonné.
        """

        if original_freq != self.rate:
            audio = torchaudio.transforms.Resample(original_freq, self.rate)(audio)

        if len(audio) > 1:
            sig = torch.mean(audio, dim=1)

        resampled = torchaudio.transforms.Resample(original_freq, self.rate,)(sig)

        return resampled

    
    def _define_speaker(self, speaker_id:int) -> str:
        """Fonction de définition du conférencier.

        Parameters
        ----------
        speaker_id : int
            Identifiant du conférencier.

        Returns
        -------
        str
            Nom du conférencier.
        """

        return self.speakers[speaker_id]

# -----------------------------------------------------------------------------
# Interface Gradio
# -----------------------------------------------------------------------------

# Création du service de microphone
input_ = gr.inputs.Audio(source="microphone", type="numpy")
# Création du service de reconnaissance
model = GradioService()

# Création de l'interface
spkr = gr.Interface(
    fn=model.inference, 
    inputs=input_, 
    outputs="text",
    title="🎤 Meetup Data Science Brest 2021 - Speaker Recognition",
)
spkr.launch(inline=False)