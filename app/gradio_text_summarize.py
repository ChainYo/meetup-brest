"""🎤 Meetup Data Science Brest 2021 - Gradio Text Summarize

Authors
  * Jean-Marie Prigent
  * Thomas Chaigneau
"""

from logging import PlaceHolder
import gradio as gr

from gradio.mix import Parallel, Series

## TRANSLATE FR2EN
translator_fr2en = gr.Interface.load("huggingface/Helsinki-NLP/opus-mt-fr-en")

summarizer_ssh = gr.Interface.load("huggingface/sshleifer/distilbart-cnn-12-6")
summarizer_peg = gr.Interface.load("huggingface/google/pegasus-xsum")

## TRANSLATE & SUMMARIZE
summariz_and_translate_ssh = Series(translator_fr2en, summarizer_ssh)
summariz_and_translate_peg = Series(translator_fr2en, summarizer_peg)

## TRANSLATE FR2EN
translator_en2fr = gr.Interface.load("huggingface/Helsinki-NLP/opus-mt-en-fr")
summariz_and_translate_ssh_en2fr = Series(summariz_and_translate_ssh, translator_en2fr)
summariz_and_translate_peg_en2fr = Series(summariz_and_translate_peg, translator_en2fr)

summarizer_cse = gr.Interface.load("huggingface/csebuetnlp/mT5_multilingual_XLSum")
summarizer_plg = gr.Interface.load("huggingface/plguillou/t5-base-fr-sum-cnndm")
summarizer_mou_abs = gr.Interface.load("huggingface/moussaKam/barthez-orangesum-abstract")
summarizer_mou_tit = gr.Interface.load("huggingface/moussaKam/barthez-orangesum-title")
summarizer_bar = gr.Interface.load("huggingface/mrm8488/camembert2camembert_shared-finetuned-french-summarization")
summarizer_lin = gr.Interface.load("huggingface/lincoln/mbart-mlsum-automatic-summarization")

# Topic Classifications
topicclassif_lin = gr.Interface.load("huggingface/lincoln/flaubert-mlsum-topic-classification")

# Sentiment analysis
sentiments_tbl = gr.Interface.load("huggingface/tblard/tf-allocine")

text_placeholder = "Le NLP (Natural Language Processing) est une branche de l’intelligence artificielle qui s’occupe particulièrement du traitement du langage écrit aussi appelé avec le nom français TALN (traitement automatique du langage naturel). En bref, c’est tout ce qui est lié au langage humain et au traitement de celui-ci par des outils informatiques. Le NLP peut être divisé en 2 grandes parties, le NLU (Natural Language Understanding) et le NLG (Natural Language Generation). Le premier est toute la partie « compréhension » du texte, prendre un texte en entrée et pouvoir en ressortir des données. Le second, est générer du texte à partir de données, pouvoir construire des phrases cohérentes de manière automatique."

# Interface for summarization
Parallel(
    summariz_and_translate_ssh, 
    summariz_and_translate_peg, 
    summariz_and_translate_ssh_en2fr,
    summariz_and_translate_peg_en2fr, 
    summarizer_cse, summarizer_plg,
    summarizer_mou_abs, 
    summarizer_mou_tit, 
    summarizer_bar, 
    summarizer_lin,
    topicclassif_lin, 
    sentiments_tbl,
    inputs=gr.inputs.Textbox(
        lines=15, 
        label="Text to summarize.",
        default=text_placeholder
    )
).launch()
