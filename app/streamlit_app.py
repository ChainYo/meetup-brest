"""🎤 Meetup Data Science Brest 2021 - Streamlit App

Authors
  * Jean-Marie Prigent
  * Thomas Chaigneau
"""

import os
import requests
import streamlit as st
import streamlit.components.v1 as components

from time import sleep
from typing import List
from dotenv import load_dotenv
from bokeh.models import CustomJS
from bokeh.models.widgets import Button
from streamlit_bokeh_events import streamlit_bokeh_events


load_dotenv()

st.set_page_config(
    layout="wide",
    page_title="Meetup Data Science",
    page_icon="📢"
)

st.title("🎤 Meetup Data Science Brest 2021 - NLP/NLU")
st.markdown("""
    ![Version](https://img.shields.io/badge/Version-0.1-green)
    ![Python](https://img.shields.io/badge/Python-3.8.10-blue)
    ![Streamlit](https://img.shields.io/badge/Streamlit-1.0.0-yellow)
    """
)

with st.expander("LANGUAGES", expanded=True):
    lang_cols = st.columns(2)
    with lang_cols[0]:
        input_lang = st.selectbox(
            label="Select input language",
            options=("🇫🇷 French",),
            index=0
        )
    with lang_cols[1]:
        output_lang = st.selectbox(
            label="Select output language",
            options=("🇫🇷 French", "🇬🇧 English", "🇪🇸 Spanish", "🇩🇪 German"),
            index=1
        )

# Record microphone logic
agent_start_btn = Button(label="Activate Vocal Assistant")
agent_start_btn.js_on_event("button_click", CustomJS(code="""
    var recognition = new webkitSpeechRecognition();
    recognition.continuous = true;
    recognition.interimResults = true;
    recognition.lang = "fr-FR";

    recognition.onresult = function (e) {
        var value = "";
        for (var i = e.resultIndex; i < e.results.length; ++i) {
            if (e.results[i].isFinal) {
                value += e.results[i][0].transcript;
            }
        }
        if ( value != "") {
            document.dispatchEvent(new CustomEvent("GET_TEXT", {detail: value}));
        }
    }
    recognition.start();
"""))

result = streamlit_bokeh_events(
    agent_start_btn,
    events="GET_TEXT",
    key="listen",
    refresh_on_update=False,
    override_height=75,
    debounce_time=0)

# A voir pour transformer en components.html pour meilleure intégration dans la page
components.html(
    """
    <div>
        <center>
            <iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRn8WW-7RMzW4sNPOI9aJOnzZ5Da1pyiYxRPtc7ejSwAG1Jvbwx7BE6kB49pYhSj8tFWTxYxFgU3IfW/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
        </center>
    </div>
    """,
    height=580
)


@st.cache(allow_output_mutation=True)
def Summary():
    return []  


def get_summary(text: List[str]) -> str:
    """Get summary"""
    text_to_summarize = "".join(text)
    summarize = requests.post(
        "https://api-inference.huggingface.co/models/sshleifer/distilbart-cnn-12-6",
        headers={"Authorization": f"Bearer {os.getenv('HUGGINGFACE_TOKEN')}"},
        json={"inputs": text_to_summarize}
    )
    st.session_state["SUMMARIZE"] = summarize.json()[0]["summary_text"]

summary = Summary()

if result:
    if "GET_TEXT" in result:
        text = result.get("GET_TEXT")
        if input_lang == "🇫🇷 French":
            if output_lang == "🇫🇷 French":
                subtitles = text
            else:
                if output_lang == "🇬🇧 English":
                    lang = "en"
                elif output_lang == "🇪🇸 Spanish":
                    lang = "es"
                elif output_lang == "🇩🇪 German":
                    lang = "de"
                output = requests.post(
                    f"https://api-inference.huggingface.co/models/Helsinki-NLP/opus-mt-fr-{lang}",
                    headers={"Authorization": f"Bearer {os.getenv('HUGGINGFACE_TOKEN')}"},
                    json={"inputs": text,}
                )
                try: 
                    subtitles = output.json()[0]["translation_text"]
                    summary.append(subtitles)
                except: subtitles = output.json()

        st.markdown(f"""
            <center>
                <h1> {subtitles} </h1>
            </center>
            """,
            unsafe_allow_html=True
        )


cols = st.columns(3)
with cols[1]:
    summarize = st.button(
        label="📝 SUMMARIZE",
        key="summarize",
        on_click=get_summary,
        args=[summary],
    )

def check_if_summary_exist():
    if st.session_state["SUMMARIZE"]:
        return True
    else:
        sleep(1)
        return check_if_summary_exist()

if summarize:
    if check_if_summary_exist():
        st.markdown(f"""
            <center>
                <h1> {st.session_state["SUMMARIZE"]} </h1>
            </center>
            """,
            unsafe_allow_html=True
        )
