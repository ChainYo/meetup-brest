## Meetup Data Science

### Enregistrer sa voix

```bash
sudo apt-add-repository ppa:audio-recorder/ppa
sudo apt-get update
sudo apt-get install audio-recorder
```

*Paramètres de audio-recorder:* format FLAC avec micro disponible

### Speaker Recognition - Speechbrain

- Notebook pour training from scratch: [lien](https://colab.research.google.com/drive/1UwisnAjr8nQF3UnrkIJ4abBMAWzVwBMh?usp=sharing)

### PyAudio

```bash
git clone https://github.com/PortAudio/portaudio
cd portaudio
./configure && make
pip install PyAudio==0.2.11
```
